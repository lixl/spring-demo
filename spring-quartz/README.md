# 时间调度的几种方式
## 1.scheduled
spring-scheduled.xml

## 2.quartz
[quartz-2.2.2 documentation](http://www.quartz-scheduler.org/downloads/files/quartz-2.2.2-distribution.tar.gz)
- 单体应用 spring-quartz.xml       
    
    SchedulerFactoryBean    
    triggers 用来指定被调度的方法,`org.springframework.scheduling.quartz.CronTriggerFactoryBean`            
    taskExecutor 线程调度器，默认实现`org.springframework.scheduling.quartz.SimpleThreadPoolTaskExecutor`

- 集群配置 spring-quartz-cluster.xml


### FAQ
- 1在spring3.1.0，quartz2.2.x环境下
启动时报错
```
 java.lang.IncompatibleClassChangeError: Found interface org.quartz.JobExecutionContext, but class was expected
```
错误原因：
spring3.1.0不支持quartz2.2.x，spring升级到`3.2.18.RELEASE`版本即可

- 2.h2database      
`java -cp h2-1.4.197.jar org.h2.tools.Server -tcpAllowOthers -webAllowOthers`       
`jdbc:h2:tcp://localhost:9092/~/test`
