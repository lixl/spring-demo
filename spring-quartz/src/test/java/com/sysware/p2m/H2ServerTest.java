package com.sysware.p2m;

import org.apache.commons.dbcp.BasicDataSource;
import org.h2.util.JdbcUtils;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.sql.*;

public class H2ServerTest {
    String driverClassName = "org.h2.Driver";
    String url = "jdbc:h2:~/dbtest;MODE=Oracle;AUTO_SERVER=TRUE";
    String username = "test";
    String password = "test";

    @Test
    public void run() throws SQLException {

        Connection connection = JdbcUtils.getConnection(driverClassName, url, username, password);
        connection.setAutoCommit(false);

        Statement stmt = connection.createStatement();
        //stmt.execute("SELECT SYSDATE FROM DUAL");

        ResultSet rs = stmt.executeQuery("SELECT SYSDATE FROM DUAL");
        while (rs.next()){
            System.out.println(rs.getObject(1));
        }
        connection.commit();
        JdbcUtils.closeSilently(rs);
        JdbcUtils.closeSilently(stmt);
        JdbcUtils.closeSilently(connection);
    }

    @Test
    public void doJdbcTemplate() throws SQLException {
        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(driverClassName);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);

        jdbcTemplate.setDataSource(dataSource);

        Object object = jdbcTemplate.queryForObject("SELECT SYSDATE FROM DUAL", Object.class);
        System.out.println(object);
        dataSource.close();

    }

}
