package com.sysware.p2m;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *  基于@Scheduled的定时任务
 * @author SPC-00D
 *
 */
@Component
public class ScheduleTaskComponent {

	
	//每3秒执行1次
	@Scheduled(cron="*/3 * * * * ?") 
	public void doTask1() {
		
		System.out.println("doTask1");
		
	}
	
}
