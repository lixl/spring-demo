package com.demo.dao;

import java.util.List;

import com.demo.entity.User;

public interface UserDao {

	public int insertUser(User user);

	public int insertUsers(List<User> users);

	public int updateUser(User user);

	public int delete(User user);
	
	public List<User> selectUsers(User user);
	
	public User selectUser(User user);

}
