package com.demo.dao.impl;

public class MemoryCalTest {
	
	private static Runtime runtime = Runtime.getRuntime();

	public static void main(String[] args) {
		int count = 10000;
		int[] datas = new int[count];
		System.out.println(datas);
		for(int i=0;i<10;i++){
			 try {
				Thread.sleep(30000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}  
			getMemory();
		}
		datas = null;
		for(int i=0;i<10;i++){
			 try {
				Thread.sleep(30000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}  
			getMemory();
		}
	}
	
	private static void getMemory(){
		System.out.println("time \t totalMemory \t freeMemory \t maxMemory \t usedMemory");
		System.out.println(System.currentTimeMillis()+"\t"+runtime.totalMemory()+"\t"+runtime.freeMemory()+"\t"+runtime.maxMemory()+"\t"+(runtime.totalMemory() - runtime.freeMemory()));
	}
	
	
	 public static void main2(String[] args) {  
		 getMemory();  
	        try {  
	            Thread.sleep(3000);  
	        } catch (Exception ee) {  
	            ee.printStackTrace();  
	        }  
	        String[] aaa = new String[2000000];  
	        getMemory();  
	        try {  
	            Thread.sleep(3000);  
	        } catch (Exception ee) {  
	            ee.printStackTrace();  
	        }  
	        for (int i = 0; i < 2000000; i++) {  
	            aaa[i] = new String("aaa");  
	        }  
	        getMemory(); 
	        try {  
	            Thread.sleep(30000);  
	        } catch (Exception ee) {  
	            ee.printStackTrace();  
	        }  
	    }  

}
