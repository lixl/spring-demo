package com.demo.entity;

import java.util.UUID;

public class User {

	private String userId = UUID.randomUUID().toString();

	private String userName = "";

	private int age = 30;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}
