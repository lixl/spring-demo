package com.demo.utils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class BeansUtil {
	
	public static Map<String, Object> toMap(Object obj){
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			Class<? extends Object> clazz = obj.getClass();
			Field[] fields = clazz.getDeclaredFields();
			if(fields!=null){
				for (int i = 0; i < fields.length; i++) {
					//设置些属性是可以访问的  
					fields[i].setAccessible(true);
					//获取属性名称
					String name = fields[i].getName();
					//获取属性值
					Object value = fields[i].get(obj);
					map.put(name, value);
				}
			}
		} catch (Exception e) {
		}
		return map;
	}
	
}
