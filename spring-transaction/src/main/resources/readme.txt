 创建用户、数据库并授权
create user 'admin'@'localhost' identified by 'admin';
create database test;
grant all on test.* to 'admin'@'localhost';

show variables like '%max_connections%'; //获取mysql客户端最大连接数

 CREATE TABLE user (
  userId VARCHAR(50) NOT NULL,
  userName VARCHAR(255) NOT NULL,
  age INT(11) NOT NULL DEFAULT 0,
  money INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (userId)
);
