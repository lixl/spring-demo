package com.demo.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Test;

import com.demo.dao.impl.JDBCUserDaoImpl;
import com.demo.entity.User;

public class JDBCUserDaoTest {

	public void insertTest(){
		User u1 = new User();
		//u1.setUserId("10001");
		u1.setAge(20);
		u1.setUserName("u1");
		
		UserDao dao = new JDBCUserDaoImpl();
		int n = dao.insertUser(u1);
		System.out.println(n);
	}
	
	@Test
	public void insertUsersTest(){
		List<User> list = new ArrayList<User>();
		UserDao dao = new JDBCUserDaoImpl();
		for(int i=0;i<100000;i++){
			User u = new User();
			u.setUserName(""+i);
			int age = new Random().nextInt();
			u.setAge(age);
			list.add(u);
		}
		
		int n = dao.insertUsers(list);
		System.out.println(n);
	}
	
}
