# 说明
## 1、HSQLDB的使用

基于jdk1.7

### jar包下载
https://repo1.maven.org/maven2/org/hsqldb/hsqldb/2.3.5/hsqldb-2.3.5.jar

通过maven访问

		<dependency>
			<groupId>org.hsqldb</groupId>
			<artifactId>hsqldb</artifactId>
			<version>2.3.5</version>
		</dependency>
		<dependency>
			<groupId>org.hsqldb</groupId>
			<artifactId>sqltool</artifactId>
			<version>2.3.5</version>
		</dependency>
### server启动
> java -cp hsqldb-2.3.5.jar org.hsqldb.server.Server -port 9001 -database.0 file:D://hsqldata/mydb -dbname.0 mydb

或者编辑配置server.properties
>server.port = 9001 ##指定服务端口

>server.database.0 = file:./data/mydb ##数据库文件硬盘路径

>server.dbname.0 = mydb ##数据库名称，通过jdbc连接时使用

>server.silent = true

>server.trace = true

>server.remote_open = true

### 启动Swing Manager Client
>java -cp hsqldb-2.3.5.jar org.hsqldb.util.DatabaseManagerSwing

### SqlTools使用
>java -cp sqltool-2.3.5.jar org.hsqldb.cmdline.SqlTool

### maven打包
>mvn clean install -Dmaven.test.skip=true

>mvn source:jar