package com.lixl.utils;

/**
 * 消息状态
 * @author Administrator
 *
 */
public enum MessageStatus{
	SENDING(1),  
	ACCEPTED(2), 
	REJECTED(3);
	private int value = 0;
	private MessageStatus(int value){
		this.value = value;
	}
	
	public int getValue(){
		return this.value;
	}
	
	@Override
	public String toString() {
		return this.name()+"-"+this.value;
	}
}
