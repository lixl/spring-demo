package com.lixl.entitys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.lixl.utils.MessageStatus;

@Entity
@Table(name = "t_message")
public class Message {
	
	@Id
	private String id;
	
	@Lob
	@Column(columnDefinition="消息内容")
	private String messageObj;
	
	@Column(columnDefinition="发送者", nullable=false)
	private Long sender;
	
	@Column(columnDefinition="接收者")
	private Long accepter;
	
	@Column(name="messageStatus", columnDefinition="消息状态")
	private MessageStatus status;

}
