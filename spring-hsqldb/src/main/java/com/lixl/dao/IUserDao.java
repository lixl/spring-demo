package com.lixl.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.lixl.entitys.User;

@Repository
public interface IUserDao extends PagingAndSortingRepository<User, Long>{

}
