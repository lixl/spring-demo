package com.lixl.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.lixl.entitys.User;
import com.lixl.service.UserServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring-jpa.xml"})
public class UserServiceTest {
	
	@Autowired
	UserServiceImpl userServiceImpl;

	@Test
	public void saveUserTest(){
		User user = new User();
		user.setUserName("张三");
		user.setPassword("123");
		user.setAddress("四川省成都市天府新区XX街DD小区FF18号");
		
		userServiceImpl.saveUser(user);
		
		User user2 = new User();
		user2.setUserName("李四");
		user2.setPassword("123");
		user2.setAddress("四川省成都市天府新区XX街ff小区FF189号");
		userServiceImpl.saveUser(user2);
		
	}

}
