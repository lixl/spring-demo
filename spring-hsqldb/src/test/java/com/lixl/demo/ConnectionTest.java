package com.lixl.demo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

public class ConnectionTest {

	private static String className = "org.hsqldb.jdbc.JDBCDriver";
	private static String url = "jdbc:hsqldb:hsql://127.0.0.1:9001/mydb";
	private static String user = "SA";
	private static String password = "";

	public static void main(String[] args) throws Exception {
		Class.forName(className);
		Connection connection = DriverManager
				.getConnection(url, user, password);
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery("select * from t_user");
		ResultSetMetaData rsmd = rs.getMetaData();
		int k = 1;
		while (rs.next()) {
			System.out.println("=======" + (k++) + "=======");
			for (int i = 1; i <= rsmd.getColumnCount(); i++) {
				String columnName = rsmd.getColumnName(i);
				System.out.println(columnName + "=" + rs.getString(i));
			}
		}
		rs.close();
		stmt.close();
		connection.close();
	}

}
