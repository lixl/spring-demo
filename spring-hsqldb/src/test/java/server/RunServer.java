package server;

/**
 * <a href="https://nchc.dl.sourceforge.net/project/hsqldb/hsqldb/hsqldb_2_4/hsqldb-2.4.0.zip">hsqldb-2.4.0</a>
 * @author Administrator
 *
 */
public class RunServer {

	public static void main(String[] args) {
		//java org.hsqldb.server.Server -port 9001 -database.0 file:../data/mydb -dbname.0 mydb
		//jdbc:hsqldb:hsql://localhost:9001/mydb
		args = "-port 9001 -database.0 file:D://hsqldata/mydb -dbname.0 mydb".split(" ");
		org.hsqldb.server.Server.main(args);
		
		//org.hsqldb.server.WebServer.main(args);
	}

}
