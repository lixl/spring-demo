package com.sysware.pm.dao.entity;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Id;

public abstract class BaseObject implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	// @GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(name="ID",unique=true)
	private String id = UUID.randomUUID().toString();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
