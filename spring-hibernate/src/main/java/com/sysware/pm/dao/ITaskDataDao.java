package com.sysware.pm.dao;

import java.util.List;

import com.sysware.pm.dao.entity.TaskDataObject;

public interface ITaskDataDao {
	
	public TaskDataObject getTaskDataObject(String id);
	
	public List<TaskDataObject> getTaskDataObjectList(String name);
	
	public void saveTaskDataObject(TaskDataObject obj);

}
