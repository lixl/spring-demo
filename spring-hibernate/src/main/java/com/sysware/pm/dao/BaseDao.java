package com.sysware.pm.dao;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;

@Service
public class BaseDao<T> extends HibernateDaoSupport {

	@Resource(name = "sessionFactory")
	public void setSuperSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	public Session getHibernateSession() {
		return getSession();
	}

	public T get(Class<T> clazz, String id) {
		return getHibernateTemplate().get(clazz, id);
	}

	public void save(Object object) {
		super.getHibernateTemplate().save(object);
	}
	
	public void delete(Object entity){
		getHibernateTemplate().delete(entity);
	}
	
	public Query createHibernateQuery(String queryString){
		return getSession().createQuery(queryString);
	}
	
	public SQLQuery createSqlQuery(String queryString){
		return getSession().createSQLQuery(queryString);
	}

}
