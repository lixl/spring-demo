package com.sysware.pm.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sysware.pm.dao.BaseDao;
import com.sysware.pm.dao.ITaskDataDao;
import com.sysware.pm.dao.entity.TaskDataObject;

@Service
public class TaskDataDaoImpl implements ITaskDataDao {
	
	@Autowired
	BaseDao<TaskDataObject> baseDao;

	@Override
	public TaskDataObject getTaskDataObject(String id) {
		return baseDao.get(TaskDataObject.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TaskDataObject> getTaskDataObjectList(String name) {
		String queryString = "from TaskDataObject where name=:name";
		List<TaskDataObject> list = baseDao.getHibernateSession().createQuery(queryString).setString("name", name).list();
		return list;
	}

	@Override
	public void saveTaskDataObject(TaskDataObject obj) {
		baseDao.save(obj);
	}

}
