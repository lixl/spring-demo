package com.sysware.pm.dto;

import java.io.Serializable;
import java.util.Date;

public class TaskData implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;

	private String name;

	private Date createTime;

	private int status;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
