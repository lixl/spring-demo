package com.sysware.pm.service;

import com.sysware.pm.dto.TaskData;

public interface DataService {
	
	public TaskData getTaskDataById(String id);
	
	public boolean saveData(TaskData data);

}
