package com.sysware.pm.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sysware.pm.dao.ITaskDataDao;
import com.sysware.pm.dao.entity.TaskDataObject;
import com.sysware.pm.dto.TaskData;
import com.sysware.pm.service.DataService;

@Service
public class DataServiceImpl implements DataService {
	
	@Autowired
	ITaskDataDao taskDataDao;

	@Override
	public TaskData getTaskDataById(String id) {
		TaskData taskData = new TaskData();
		taskData.setCreateTime(new Date());
		taskData.setId(id);
		return taskData;
	}

	@Override
	public boolean saveData(TaskData data) {
		TaskDataObject obj = new TaskDataObject();
		obj.setName(data.getName());
		taskDataDao.saveTaskDataObject(obj);
		return true;
	}

	
	

}
