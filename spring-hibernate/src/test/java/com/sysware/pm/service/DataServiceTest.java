package com.sysware.pm.service;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sysware.pm.dto.TaskData;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:application_core.xml" })
public class DataServiceTest extends AbstractJUnit4SpringContextTests{
	
	@Autowired
	DataService dataService;
	
	Logger logger = Logger.getLogger(DataServiceTest.class);
	
	@Test
	public void getTaskDataByIdTest(){
		TaskData taskdata = dataService.getTaskDataById("1213");
		logger.debug(taskdata);
		Assert.assertNotEquals(null, taskdata);
	}
	
	@Test
	public void saveDataTest(){
		TaskData taskdata = new TaskData();
		taskdata.setName("张三");
		dataService.saveData(taskdata);
	}

}
