package com.sysware.pm;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ServiceMain {

	private static final Logger logger = Logger.getLogger(ServiceMain.class);

	public static void main(String[] args) throws IOException {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				"application_core.xml");

		context.start();
		logger.info("spring start success");

		System.in.read();
		context.close();
	}

}
