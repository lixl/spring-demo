package com.sysware.p2m.cache;

import org.apache.commons.lang.StringUtils;

public class P2MCacheFactory {

	public final static String EHCACHE_NAME = "ehcache";

	public final static String OSCACHE_NAME = "oscache";

	public final static String MEMCACHE_NAME = "memcache";

	public static <K, V> P2MCache<K, V> getCacheInstance() {

		P2MCache<K, V> cache = new DefaultCache<>();

		return cache;
	}

	public static <K, V> P2MCache<K, V> getCacheInstance(String name) {
		if (StringUtils.isEmpty(name)) {
			return getCacheInstance();
		}

		if (EHCACHE_NAME.equals(name)) {
			return getEhcacheInstance();
		}

		if (OSCACHE_NAME.equals(name)) {
			return getOscacheInstance();
		}

		if (MEMCACHE_NAME.equals(name)) {
			return getMemcacheInstance();
		}

		return null;
	}

	private static <K, V> P2MCache<K, V> getEhcacheInstance() {
		return getCacheInstance();
	}

	private static <K, V> P2MCache<K, V> getOscacheInstance() {
		return getCacheInstance();
	}

	private static <K, V> P2MCache<K, V> getMemcacheInstance() {
		return getCacheInstance();
	}

}
