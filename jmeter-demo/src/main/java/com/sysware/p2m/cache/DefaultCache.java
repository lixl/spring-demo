package com.sysware.p2m.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class DefaultCache<K, V> implements P2MCache<K, V> {

	private ConcurrentHashMap<K, V> CACHEMAP = new ConcurrentHashMap<>();

	@Override
	public void put(K poKey, V poValue) {
		CACHEMAP.put(poKey, poValue);
	}

	@Override
	public V get(K poKey) {
		return CACHEMAP.get(poKey);
	}

	@Override
	public boolean remove(K poKey) {
		CACHEMAP.remove(poKey);
		return true;
	}

	@Override
	public Map<K, V> getKeyValueMap() {
		return CACHEMAP;
	}

	@Override
	public Map<K, V> getKeyValueMap(String regex) {
		Map<K, V> map = new HashMap<>();
		Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		for (Map.Entry<K, V> e : CACHEMAP.entrySet()) {
			K key = e.getKey();
			if (key instanceof String && key != null) {
				Matcher m = p.matcher(key.toString());
				if (m.matches()) {
					map.put(key, e.getValue());
				}
			}
		}
		return map;
	}
}
