package com.sysware.p2m.cache.intercepter;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.cache.interceptor.DefaultKeyGenerator;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import com.alibaba.fastjson.JSONObject;
import com.sysware.p2m.cache.P2MCache;
import com.sysware.p2m.cache.P2MCacheFactory;
import com.sysware.p2m.cache.annotation.CacheConfig;

/**
 * 
 * 拦截有CacheConfig注解的方法并缓存接口数据
 * 
 * @author spc-00d
 *
 */
@Aspect
@Component
public class P2MCahceAnnotationIntercepter {

	private Logger logger = Logger.getLogger(getClass());

	/**
	 * 默认检查时间 5秒一次
	 */
	private static final long checkPeriodTime = 5000;

	public P2MCahceAnnotationIntercepter() {
		// 启动后台定时检查缓存任务
		new Timer(true).schedule(new CacheExpireChecker(), 1, checkPeriodTime);
	}

	/**
	 * 缓存时间key值
	 */
	private static final String CACHETIME_KEY = "CACHETIME_KEY";
	/**
	 * 缓存数据key值
	 */
	private static final String CACHEDATA_KEY = "CACHEDATA_KEY";

	/**
	 * 缓存配置时间key值
	 */
	private static final String CACHECONFIG_EXPIRETIME_KEY = "CACHECONFIG_EXPIRETIME_KEY";

	/**
	 * 拦截com.sysware.p2m.cache.annotation.CacheConfig方法
	 * 
	 * @param joinPoint
	 * @return
	 */
	@Around("@annotation(com.sysware.p2m.cache.annotation.CacheConfig)")
	//@Around("execution (* com.sysware.p2m.system.service.impl..*.*(..))")
	public Object around(ProceedingJoinPoint joinPoint) {
		StopWatch sw = new StopWatch();
		sw.start();
		// 被拦截的方法参数
		Class<? extends Object> target = joinPoint.getTarget().getClass();

		// 被拦截的方法名称
		MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();

		Method method = methodSignature.getMethod();

		Object cacheKey = generateKey(target, method, joinPoint.getArgs());

		CacheConfig cacheConfig = AnnotationUtils.findAnnotation(method, CacheConfig.class);

		/**
		 * 生成缓存名称，默认为：类名#方法名
		 */
		String cacheName = target.getName() + "#" + method.getName();
		long expireTime = 60000;
		if (cacheConfig != null) {
			cacheName = cacheConfig.value();
			expireTime = cacheConfig.expireTime();
		}

		ConcurrentHashMap<Object, JSONObject> cacheContent = getConcurrentMapCache(cacheName);

		JSONObject valueWrapper = cacheContent.get(cacheKey);
		Object obj = null;
		/**
		 * 1、命中缓存
		 */
		if (valueWrapper != null) {
			// 设置时间之和大于当前时间的话，就没过期
			if (System.currentTimeMillis() - valueWrapper.getLong(CACHETIME_KEY) < expireTime) {
				obj = valueWrapper.get(CACHEDATA_KEY);
			}
		}

		/**
		 * 2、没有命中缓存(或者已过期)，重新获取并缓存
		 */
		if (obj == null) {
			obj = getAndCacheData(joinPoint, cacheContent, cacheKey, expireTime);
		}

		sw.stop();

		logger.info(target.getName() + "#" + method.getName() + " time:" + sw.getTotalTimeMillis());
		return obj;
	}

	private Object getAndCacheData(ProceedingJoinPoint joinPoint, ConcurrentHashMap<Object, JSONObject> cacheContent,
			Object cacheKey, long expireTime) {
		Object obj = null;
		try {
			obj = joinPoint.proceed();
		} catch (Throwable e) {
		}
		doCache(cacheContent, cacheKey, expireTime, obj);
		return obj;
	}

	/**
	 * 存储所有缓存数据
	 */
	private static P2MCache<String, ConcurrentHashMap<Object, JSONObject>> ALL_CACHE_DATA = P2MCacheFactory
			.getCacheInstance();

	/**
	 * 获取缓存空间(没用的话就新建)
	 * 
	 * @param cacheName
	 * @return
	 */
	private static ConcurrentHashMap<Object, JSONObject> getConcurrentMapCache(String cacheName) {
		ConcurrentHashMap<Object, JSONObject> cache = ALL_CACHE_DATA.get(cacheName);
		if (cache == null) {
			cache = new ConcurrentHashMap<Object, JSONObject>();
			ALL_CACHE_DATA.put(cacheName, cache);
		}
		return cache;
	}

	/**
	 * 缓存数据
	 */
	private void doCache(ConcurrentHashMap<Object, JSONObject> cache, Object cacheKey, long expireTime, Object obj) {
		if (cacheKey == null || obj == null)
			return;
		cache.put(cacheKey, generateValue(expireTime, obj));
	}

	private static final DefaultKeyGenerator keyGenerator = new DefaultKeyGenerator();

	/**
	 * 生成cache key值
	 * 
	 * @return
	 */
	private static Object generateKey(Class<? extends Object> target, Method method, Object[] params) {
		return keyGenerator.generate(target, method, params);
	}

	/**
	 * 生成Value值
	 * 
	 * @param obj
	 * @return
	 */
	private static JSONObject generateValue(long expireTime, Object obj) {
		JSONObject cacheData = new JSONObject();
		cacheData.put(CACHEDATA_KEY, obj);
		cacheData.put(CACHETIME_KEY, System.currentTimeMillis());
		cacheData.put(CACHECONFIG_EXPIRETIME_KEY, expireTime);
		return cacheData;
	}

	static class CacheExpireChecker extends TimerTask {
		private Logger logger = Logger.getLogger(getClass());

		@Override
		public void run() {
			Map<String, ConcurrentHashMap<Object, JSONObject>> map = ALL_CACHE_DATA.getKeyValueMap();
			for (Map.Entry<String, ConcurrentHashMap<Object, JSONObject>> e : map.entrySet()) {
				if (e == null || e.getKey() == null || e.getValue() == null || e.getValue().size() < 1)
					continue;
				ConcurrentHashMap<Object, JSONObject> fmap = e.getValue();
				int numA = fmap.size();
				for (Map.Entry<Object, JSONObject> f : fmap.entrySet()) {
					if (f == null || f.getKey() == null || f.getValue() == null)
						continue;
					// 清理过期数据
					if (System.currentTimeMillis() - f.getValue().getLong(CACHETIME_KEY) > f.getValue()
							.getLong(CACHECONFIG_EXPIRETIME_KEY)) {
						fmap.remove(f);
					}
				}
				int numB = numA - fmap.size();
				if (numB > 0) {
					logger.info("remove expire data " + e.getKey() + "\t size:" + numB + " \t time:"
							+ System.currentTimeMillis());
				}
			}
		}

	}

}
