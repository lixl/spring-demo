package com.sysware.p2m.cache;

import java.util.Map;

/**
 * 
 * 
 * Cache内容存取接口
 * 
 * @author lixl
 * @date 2018年5月18日 下午2:52:22
 */
public interface P2MCache<K, V> {

	public void put(K poKey, V poValue);

	public V get(K poKey);

	public boolean remove(K poKey);

	public Map<K, V> getKeyValueMap();

	public Map<K, V> getKeyValueMap(String regex);
	
}
