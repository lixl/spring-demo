package com.sysware.p2m.cache.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 可自动刷新的缓存配置
 * @author spc-00d
 *
 */
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface CacheConfig {

	/**
	 * 缓存对象名称
	 */
	String value() default "default";
	
	/**
	 * 缓存对象key
	 */
	String key() default "";
	
	/**
	 * 缓存过期时间(默认1 min)
	 */
	long expireTime() default 60000;
	
}
