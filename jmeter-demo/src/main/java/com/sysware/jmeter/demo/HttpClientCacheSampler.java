package com.sysware.jmeter.demo;

import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.AbstractJavaSamplerClient;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.log.Logger;

public class HttpClientCacheSampler extends AbstractJavaSamplerClient {

	private Logger log = getLogger();

	@Override
	public SampleResult runTest(JavaSamplerContext arg0) {
		log.info("execute runTest...");
		
		SampleResult results = new SampleResult();
		
		String testStr = arg0.getParameter("testStr", "");
		if (testStr.length() < 5) {
			results.setSuccessful(false);
		} else {
			results.setSuccessful(true);
		}
		return results;
	}


	@Override
	public void setupTest(JavaSamplerContext arg0) {
		log.info("execute setupTest...");
		SampleResult results = new SampleResult();
		String testStr = arg0.getParameter("testStr", "");
		if (testStr != null && testStr.length() > 0) {
			results.setSamplerData(testStr);
		}
	}

	@Override
	public Arguments getDefaultParameters() {
		log.info("execute getDefaultParameters...");
		Arguments params = new Arguments();
		params.addArgument("testStr", "");
		return params;
	}

	@Override
	public void teardownTest(JavaSamplerContext arg0) {

	}

}
