<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ckeditor</title>
<script type="text/javascript" src="./ckeditor.js"></script>
<script type="text/javascript" src="./config.js"></script>
</head>
<body>
	<div>
		<button onclick="save();">SAVE</button>
		<div id="editor">
			<h1>Hello world!</h1>
		</div>
	</div>
	<script type="text/javascript">
		CKEDITOR.replace('editor');
		
		CKEDITOR.instances.editor.on('change', function (e) {
			var a = e.editor.document ;
			var b = a.find("img");
			var count = b.count();
			if(count<1) return;
			 for(var i=0;i<count;i++){
                 var src =b.getItem(i).$.src;//获取img的src
                 console.log(src);
			 }
			console.log(count);
		});
		
		function save(){
			var data = CKEDITOR.instances.editor.getData();
			console.log(data);
		}
	</script>
</body>
</html>