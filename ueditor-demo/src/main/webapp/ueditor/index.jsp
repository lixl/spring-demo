<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="./lib/third-party/jquery-1.10.2.min.js"></script>
<script type="text/javascript" charset="utf-8"
	src="./lib/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8"
	src="./lib/ueditor.all.min.js">
	
</script>
<script type="text/javascript" charset="utf-8"
	src="./lib/lang/zh-cn/zh-cn.js"></script>
<title>富文本编辑器</title>
<script type="text/javascript">
	$(function() {
		var ue = UE.getEditor('editor');
		ue.ready(function() {
			$.ajax({
				url : "../SaveDataServlet",
				dataType : "html",
				success : function(data) {
					ue.setContent(data);
				}
			});
		});

		$("#saveBtn").on("click", function() {
			var html = ue.getContent();
			console.log(html);
			alert(html);
			$.ajax({
				url : "../SaveDataServlet",
				type : "post",
				data : {
					"type" : "save",
					"STATIC_STRING" : html
				},
				success : function() {
					//alert("save success");
				}
			});
		});
	});
</script>
</head>
<body>
	<h2>
		<a href="http://ueditor.baidu.com/website/onlinedemo.html"
			target="_blank">演示</a> <a
			href="http://fex.baidu.com/ueditor/#api-common" target="_blank">文档</a>
	</h2>
	<button id="saveBtn">保存</button>
	<script id="editor" type="text/plain"
		style="width: 1024px; height: 500px;">
 </script>
</body>
</html>