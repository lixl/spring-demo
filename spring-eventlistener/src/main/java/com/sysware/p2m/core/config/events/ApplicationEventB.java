package com.sysware.p2m.core.config.events;

public class ApplicationEventB implements ICanReloadBean {

	@Override
	public void load() {
		System.out.println("ApplicationEventB load...");
		try {
			Thread.sleep(1000L);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(12);
	}

	@Override
	public void reload() {
		System.out.println("ApplicationEventB reload...");
	}


}
