package com.sysware.p2m.core.config.events;

public class ApplicationEventA implements ICanReloadBean {

	@Override
	public void load() {
		System.out.println("ApplicationEventA load...");
		try {
			Thread.sleep(1000L);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(12);
	}

	@Override
	public void reload() {
		System.out.println("ApplicationEventA reload...");
	}


}
