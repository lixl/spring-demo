package com.sysware.p2m.core.config.events;

public interface ICanReloadBean {

	public void load();
	
	public void reload();
	
}
