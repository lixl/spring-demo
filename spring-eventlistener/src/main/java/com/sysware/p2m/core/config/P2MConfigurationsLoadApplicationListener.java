package com.sysware.p2m.core.config;

import java.util.Map;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import com.sysware.p2m.core.config.events.ICanReloadBean;

public class P2MConfigurationsLoadApplicationListener implements ApplicationListener<ContextRefreshedEvent> {

	private boolean autoLoad;

	public boolean isAutoLoad() {
		return autoLoad;
	}

	public void setAutoLoad(boolean autoLoad) {
		this.autoLoad = autoLoad;
	}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		Map<String, ICanReloadBean> beans = event.getApplicationContext().getBeansOfType(ICanReloadBean.class);
		System.out.println(beans);
		if (beans != null && !beans.isEmpty()) {
			for (Map.Entry<String, ICanReloadBean> bean : beans.entrySet()) {
				if (bean != null && bean.getValue() != null) {
					bean.getValue().load();
				}
			}
		}
	}

}
