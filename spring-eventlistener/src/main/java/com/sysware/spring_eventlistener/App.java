package com.sysware.spring_eventlistener;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) throws IOException {
		// System.out.println("Hello World!");
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext();
		context.setConfigLocations(new String[] { "classpath:spring/application_*.xml" });

		context.refresh();

		InputStream is = System.in;
		while (is.read() != -1) {
			break;
		}

	}
}
