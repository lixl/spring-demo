<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${bookName}</title>
</head>
<body>
	<table border="1" cellpadding="2" cellspacing="2" align="center">
		<tr>
			<td height="30" colspan="2">
				<h3>${bookName}</h3><b>${author} 著</b>
			</td>
		</tr>
		<tr>
			<td height="250" width="200">
				<h4>目录</h4>
				<ul>
					<c:forEach items="${bookMenu}" var="m">
						<li>${m.name}</li>
					</c:forEach>
				</ul>
			</td>
			<td width="600">
				<tiles:insertAttribute name="body" />
			</td>
		</tr>
		<tr>
			<td height="30" colspan="2">
				<tiles:insertAttribute name="footer" />
			</td>
		</tr>
	</table>
</body>
</html>