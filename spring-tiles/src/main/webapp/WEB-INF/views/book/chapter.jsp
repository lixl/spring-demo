<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<p>
	错误：指定的日志文件中没有输出日志，也没有报错
原因：配置文件中使用了自定义名称的方式，但是类中调用日志文件的时候却使用的类名
解决：如果类是个jar包，不容许修改的，那么就把配置文件改了；具体如下：        log4j.logger.com.hudong.keel.web.filter.PerformanceMonitorFilter=INFO,performanceLogger     
log4j.appender.performanceLogger=org.apache.log4j.DailyRollingFileAppender
 如果类可以改，那也可以不改配置文件直接该类，如下：
   private final static Log log = LogFactory.getLog (“performanceLogger”);
扩展：如果你不想在rootLogger指定的文件下输出内容，只想在你知道的自定义文件下输出你的日志，那么你可以把输出开关改为false，默认的是true；如果你不该那么就会在跟日志和你自定义的日志都输出日志；开关如下:
 log4j.additivity.createExcel = false
	</p>