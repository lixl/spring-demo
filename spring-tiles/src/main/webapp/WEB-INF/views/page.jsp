<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>page</title>
</head>
<body>
	page.jsp

	<table>
	<c:forEach items="${datas}" var="data">
		<tr>
			<td>${data.name}</td> 
			<td>${data.id}</td>
			<td><fmt:formatDate value="${data.createTime}" pattern="yyyy-MM-dd"/></td> 
			<td>¥ <fmt:formatNumber type="number" value="${data.price}" pattern="0.00" maxFractionDigits="2"/></td>
		</tr>
	</c:forEach>
</table>
</body>
</html>