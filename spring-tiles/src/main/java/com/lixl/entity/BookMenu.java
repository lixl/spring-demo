package com.lixl.entity;

import java.util.List;

public class BookMenu {
	/**
	 * 序号
	 */
	private String num;
	
	/**
	 * 目录名称
	 */
	private String name;
	
	/**
	 * 子目录
	 */
	private List<BookMenu> subMenu;

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<BookMenu> getSubMenu() {
		return subMenu;
	}

	public void setSubMenu(List<BookMenu> subMenu) {
		this.subMenu = subMenu;
	}
}
