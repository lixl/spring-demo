package com.lixl.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lixl.entity.BookMenu;

@Controller
@RequestMapping("hello")
public class HelloController {

	@RequestMapping(value = "say", method = RequestMethod.GET)
	@ResponseBody
	public String sayHello(@RequestParam String name) {
		return "hello " + name;
	}
	
	@RequestMapping(value = "info", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> getInfo(@RequestParam String name) {
		int id = new Random().nextInt();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", name);
		map.put("id", Math.abs(id));
		return map;
	}

	@RequestMapping(value = "page")
	public String toHelloPage(Model model) {
		List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
		for (int i = 0; i < 10; i++) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", Math.abs(new Random().nextInt()));
			map.put("name", "詹三"+i);
			map.put("createTime", new Date());
			map.put("price", Math.abs(new Random().nextDouble()));
			list.add(map);
		}
		model.addAttribute("datas", list);
		return "page";
	}
	
	@RequestMapping(value = "template")
	public String template(Model model) {
		List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
		for (int i = 0; i < 10; i++) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", Math.abs(new Random().nextInt()));
			map.put("name", "詹三"+i);
			map.put("createTime", new Date());
			map.put("price", Math.abs(new Random().nextDouble()));
			list.add(map);
		}
		model.addAttribute("datas", list);
		return "base.definition";
	}
	
	@RequestMapping(value = "book", method = RequestMethod.GET)
	public String getBook(@RequestParam String bookId, Model model){
		model.addAttribute("bookId", bookId);
		model.addAttribute("bookName", "射雕英雄传");
		model.addAttribute("author", "金庸");
		
		model.addAttribute("bookMenu", getBookMenuList());
		
		return "book.definition";
	}
	
	private List<BookMenu> getBookMenuList(){
		List<BookMenu> bookMenu = new ArrayList<BookMenu>();
		BookMenu m1 = new BookMenu();
		m1.setNum("1");
		m1.setName("第一回 风雪惊变");
		bookMenu.add(m1);
		
		BookMenu m2 = new BookMenu();
		m2.setNum("2");
		m2.setName("第二回 江南七怪");
		bookMenu.add(m2);
		
		BookMenu m3 = new BookMenu();
		m3.setNum("3");
		m3.setName("第三回 大漠风沙");
		bookMenu.add(m3);
		
		BookMenu m4 = new BookMenu();
		m4.setNum("4");
		m4.setName("第四回 黑风双煞");
		bookMenu.add(m4);
		return bookMenu;
	}
	

}
