package com.commons.collections4;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections4.map.FixedSizeMap;
import org.apache.commons.collections4.map.UnmodifiableMap;

public class Test {
	
	public static void main(String[] args) {
		HashMap<String, String> map = new HashMap<>();
		FixedSizeMap<String, String> fsm = FixedSizeMap.fixedSizeMap(map);
		fsm.mapIterator();
		
		map.put("12", "ddd");
		
		Map<String, String> umap = UnmodifiableMap.unmodifiableMap(map);
		umap.put("13", "343");
		
		System.out.println(umap);
	}

}
