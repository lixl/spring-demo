package com.commons.lang2;

import org.apache.commons.lang.StringUtils;

public class Test {

	public static void main(String[] args) {
		
		String text = "select * from table where id=:id and name=:name";
		String[] searchList = new String[] {":id", ":name"};
		String[] replacementList = new String[] {"'10001'", "'战犯三'"};
		String str = StringUtils.replaceEach(text, searchList, replacementList);

		System.out.println(str);
		
	}

}
