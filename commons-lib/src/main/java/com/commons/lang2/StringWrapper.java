package com.commons.lang2;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.math.NumberUtils;

/**
 * String的装饰类，用于提供参数替换的功能
 * @version : 1.0
 * @since : 2013-9-13上午9:26:39
 * @team : P2M
 * @author : suny
 */
public class StringWrapper {

    private StringBuilder buffer;

    private String namedParameterPrefix = ":";

    private String regex = "[{]\\d*[}]";

    /**
     * 描述 : 设置正则表达式
     * 
     * @since : 2013-9-13:上午9:28:17
     * @author : suny
     * @param regex
     */
    public void setRegex(String regex) {
        this.regex = regex;
    }

    public StringWrapper(String str) {
        buffer = new StringBuilder(str);
    }
    
    public StringWrapper(StringBuilder s){
    	buffer = s;
    }

    private Map<String, Object> namedParameter = new HashMap<String, Object>();

    public StringWrapper setParameter(String namedParameter, Object value) {
        this.namedParameter.put(namedParameter, value);
        return this;
    }

    public StringWrapper setProperties(Map<String, Object> map) {
        namedParameter = map;
        return this;
    }

    /**
     * 描述 : 格式化字符串
     * 
     * @since : 2013-9-13:上午9:31:07
     * @author : suny
     * @param value
     * @return
     */
    public String format(Object... value) {
        if (namedParameter.size() > 0) {
            Set<String> keySet = namedParameter.keySet();
            for (String key : keySet) {
                Object objValue = namedParameter.get(key);
                String parameterName = namedParameterPrefix + key;
                buffer = new StringBuilder(buffer.toString().replace(parameterName, String.valueOf(objValue)));
            }
        }
        int len = value.length;
        if (len == 0)
            return buffer.toString();
        StringBuffer sb = new StringBuffer();
        Matcher matcher = Pattern.compile(regex).matcher(buffer);
        while (matcher.find()) {
            String group = matcher.group();
            int index = getIndex(group);
            if (index > len - 1)
                throw new RuntimeException("format的输入参数没有第" + (index + 1) + "个参数值！");
            Object objValue = value[index];
            matcher.appendReplacement(sb, String.valueOf(objValue));
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    /**
     * 描述 : 将通过正则表达式match到的字符串解析，得到要替换的输入参数的index
     * 
     * @since : 2013-9-13:上午9:28:36
     * @author : suny
     * @param group 通过正则表达式match到的字符串
     * @return
     */
    public int getIndex(String group) {
        if (group == null || group.trim().equals(""))
            throw new RuntimeException("regex匹配的group为空！");
        int len = group.length();
        return NumberUtils.toInt(group.substring(1, len - 1));
    }

    @Override
    public String toString() {
        return this.format();
    }

    public static void main(String[] args) {
        String ttt = "asdfasd{0}adfadfaf{0}asdasdf{1}gfdagf{2}:abc";
        StringWrapper w = new StringWrapper(ttt);
        System.out.println(w.setParameter("abc", "AAAA").format(1, 2, 3));
    }
}
