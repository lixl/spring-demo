package com.commons.cli;

import org.junit.Test;

public class RMDataSourceTest {

	// 测试带有 –h 参数的代码功能
	@Test
	public void testHelp() {
		String args[] = { "-h" };
		RMDataSource.simpleTest(args);
	}

	// 测试没有带 –h 参数的代码功能
	@Test
	public void testNoArgs() {
		String args[] = new String[0];
		RMDataSource.simpleTest(args);
	}

	// 测试输入所有正确参数的代码功能
	@Test
	public void testRMDataSource() {
		/**
		 * 此字符串参数等同于在命令行窗口输入命令 java rmdatasource -i 192.168.0.2 -p 5988 -t http
		 */
		String args[] = new String[] { "-i", "192.168.0.2", "-p", "5988", "-t", "http" };
		RMDataSource.simpleTest(args);
	}

}
