# 时间调度的几种方式
## 1.scheduled
spring-scheduled.xml

## 2.quartz
[quartz-1.8.6 documentation](http://www.quartz-scheduler.org/documentation/quartz-1.8.6/index.html)
- 单体应用 spring-quartz.xml       
    
    SchedulerFactoryBean    
    triggers 用来指定被调度的方法,`org.springframework.scheduling.quartz.CronTriggerFactoryBean`            
    taskExecutor 线程调度器，默认实现`org.springframework.scheduling.quartz.SimpleThreadPoolTaskExecutor`

- 集群配置 spring-quartz-cluster.xml


