package com.sysware.p2m;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-quartz.xml")
public class QuartzTaskComponentTest {

    @Test
    public void execute() throws IOException {
        System.in.read();
    }

}
