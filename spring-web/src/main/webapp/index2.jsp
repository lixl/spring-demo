<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"> 
	<title>Bootstrap 实例 - 基本的表格</title>
	<link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">  
	<link rel="stylesheet" href="./fileupload/css/jquery.fileupload.css">
	<link rel="stylesheet" href="./fileupload/css/jquery.fileupload-ui.css">
	<script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
	<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$.ajax({
				url:"./userAction/getDatas.action",
				dataType:"json",
				success:function(datas){
					if(datas){
						for(var i=0;i<datas.length;i++){
							var name=datas[i].name;
							var oname=decodeURI(datas[i].oname);
							$("#contents").append("<tr><td><a href='./"+name+"'>"+oname+"</td><td></td></tr>");
						}
					}
				}
			});
		})
	</script>
</head>
<body>

	<div class="container">
		<div class="row fileupload-buttonbar">
		
			<span class="btn btn-success fileinput-button"> 
				<i class="glyphicon glyphicon-plus"></i> 
				<span>选择文件...</span> 
				<input type="file" name="files[]" multiple>
			</span>
		</div>
	</div>

	<table class="table">
	<caption>基本的表格布局</caption>
   <thead>
      <tr>
         <th>名称</th>
         <th>城市</th>
      </tr>
   </thead>
   <tbody id="contents">
   </tbody>
</table>

</body>
</html>