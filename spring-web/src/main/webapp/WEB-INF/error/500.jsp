<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isErrorPage="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>服务器异常</title>
</head>
<body>
	<h1 style="color: red;">500</h1>
	<div>
		<div>系统执行发生错误，信息描述如下：</div>
		<div>错误状态代码是：${pageContext.errorData.statusCode}</div>
		<div>错误发生页面是：${pageContext.errorData.requestURI}</div>
		<div>错误信息：${pageContext.exception}</div>
		<div>
			错误堆栈信息：<br />
			<c:forEach var="trace" items="${pageContext.exception.stackTrace}">
				<p>${trace}</p>
			</c:forEach>
		</div>
	</div>
</body>
</html>