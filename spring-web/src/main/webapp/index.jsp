<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
request.setAttribute("basePath", basePath);
%>
<!DOCTYPE html>
<html>
<head>
	<title>spring-web</title>
	<script type="text/javascript" src="http://apps.bdimg.com/libs/jquery/1.9.0/jquery.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$("#getDatas").on("click", function(){
				$.ajax({
					url:"${basePath}userAction/getDatas.action",
					success:function(data){
						console.log(data);
					}
				});
			});
		})
	</script>
</head>
<body>
	<h2>spring-web!</h2>
	
	<button id="getDatas">getDatas23</button>
</body>
</html>
