!function(e) {
    function t(n) {
        if (r[n]) return r[n].exports;
        var a = r[n] = {
            exports: {},
            id: n,
            loaded: !1
        };
        return e[n].call(a.exports, a, a.exports, t), a.loaded = !0, a.exports;
    }
    var r = {};
    return t.m = e, t.c = r, t.p = "", t(0);
}({
    0: function(module, exports, __webpack_require__) {
        "use strict";
        function _interopRequireDefault(e) {
            return e && e.__esModule ? e : {
                "default": e
            };
        }
        var _XmlUtils = __webpack_require__(188), _XmlUtils2 = _interopRequireDefault(_XmlUtils);
        !function() {
            function evaluate(e) {
                var variables = {}, result = {}, data = e.data;
                data.script = "(function() {\n" + (data.script || "") + "\n})();";
                var boomerang = {
                    getRequest: function(e) {
                        return e ? data.requests[e] : data.request;
                    },
                    setEnvironmentVariable: function(e, t) {
                        variables[e] = t;
                    },
                    clearEnvironmentVariable: function(e) {
                        variables[e] = null;
                    },
                    getEnvironment: function() {
                        return data.environment;
                    },
                    setNextRequest: function(e) {
                        var t = arguments.length <= 1 || void 0 === arguments[1] ? 0 : arguments[1];
                        result.nextRequest = {
                            name: e,
                            delay: t
                        };
                    },
                    xPath: function(e, t, r, n) {
                        var a;
                        a = "string" == typeof e ? new DOMParser().parseFromString(e, "text/xml") : e;
                        var o = _XmlUtils2["default"].xPathAny(t, a, null, r);
                        return o instanceof Document ? this.xml2Json(o, n) : o;
                    },
                    xPathIgnoreNS: function(e, t) {
                        var r = _XmlUtils2["default"].removeNS(e);
                        return this.xPath(r, t);
                    },
                    xml2Json: function(e, t) {
                        return _XmlUtils2["default"].xml2json(e, t);
                    },
                    uuid: function() {
                        var e = Date.now();
                        return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(t) {
                            var r = (e + 16 * Math.random()) % 16 | 0;
                            return e = Math.floor(e / 16), ("x" == t ? r : 3 & r | 8).toString(16);
                        });
                    }
                };
                result.error = function(code) {
                    var result, variables, data, e, XmlUtils;
                    try {
                        eval(code);
                    } catch (ex) {
                        return console.log(ex), ex.message;
                    }
                }(data.script), result.requestId = data.requestId, result.serviceId = data.serviceId, 
                result.variables = variables, data.debug === !0 ? (result.debug = !0, console.log("variables:", result.variables), 
                e.source.postMessage(result, event.origin)) : data.preview ? (result.preview = data.preview, 
                e.source.postMessage(result, event.origin)) : e.source.postMessage(result, event.origin);
            }
            window.addEventListener("message", function(e) {
                evaluate(e);
            });
        }();
    },
    188: function(e, t) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var r = {
            parse: function(e) {
                var t = null;
                if (window.DOMParser) try {
                    t = new DOMParser().parseFromString(e, "text/xml");
                } catch (r) {
                    t = null;
                } else if (window.ActiveXObject) try {
                    t = new ActiveXObject("Microsoft.XMLDOM"), t.async = !1, t.loadXML(e) || console.error(t.parseError.reason + t.parseError.srcText);
                } catch (r) {
                    t = null;
                } else console.error("could not parse xml string.");
                return t;
            },
            xPath: function(e, t, r) {
                function n(e, t) {
                    t || (t = []);
                    for (var r = 0; r < e.snapshotLength; r++) {
                        var n = e.snapshotItem(r);
                        t.push(n);
                    }
                    return t;
                }
                r || (r = t);
                var a = t.createNSResolver(t.documentElement), o = t.evaluate(e, r, a, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null), l = n(o);
                return l;
            },
            xPathAny: function(e, t, r, n) {
                function a(e) {
                    var t = [], n = document.implementation.createDocument("", "", null);
                    n.appendChild(n.createElement("root"));
                    for (var a = n.documentElement; r = e.iterateNext(); ) t.push(r);
                    return t.forEach(function(e) {
                        a.appendChild(e);
                    }), n;
                }
                r || (r = t);
                var o = t.createNSResolver(t.documentElement);
                n && Object.keys(n).length > 0 && (o = function(e) {
                    var t = n[e];
                    return t || n["default"];
                });
                var l = t.evaluate(e, r, o, XPathResult.ANY_TYPE, null);
                switch (l.resultType) {
                  case XPathResult.BOOLEAN_TYPE:
                    return res.booleanValue;

                  case XPathResult.NUMBER_TYPE:
                    return res.numberValue;

                  case XPathResult.STRING_TYPE:
                    return res.stringValue;

                  default:
                    return a(l);
                }
            },
            xml2json: function(e, t) {
                function r(e) {
                    var t = null, o = 0, l = "";
                    if (e.hasAttributes && e.hasAttributes()) for (t = {}, o; o < e.attributes.length; o++) {
                        var s = e.attributes.item(o);
                        t["@" + s[a]] = n(s.value.trim());
                    }
                    if (e.hasChildNodes()) for (var u, i, c, d = 0; d < e.childNodes.length; d++) u = e.childNodes.item(d), 
                    4 === u.nodeType ? l += u.nodeValue : 3 === u.nodeType ? l += u.nodeValue.trim() : 1 === u.nodeType && (0 === o && (t = {}), 
                    i = u[a], c = r(u), t.hasOwnProperty(i) ? (t[i].constructor !== Array && (t[i] = [ t[i] ]), 
                    t[i].push(c)) : (t[i] = c, o++));
                    return l && (o > 0 ? t.keyValue = n(l) : t = n(l)), t;
                }
                function n(e) {
                    return /^\s*$/.test(e) ? null : /^(?:true|false)$/i.test(e) ? "true" === e.toLowerCase() : isFinite(e) ? parseFloat(e) : e;
                }
                var a = "localName";
                t === !0 && (a = "nodeName");
                var o;
                return o = "string" == typeof e ? new DOMParser().parseFromString(e, "text/xml") : e, 
                r(o.documentElement);
            },
            removeNS: function(e) {
                function t(e) {
                    var r = n.createElement(e.localName);
                    if (e.hasAttributes && e.hasAttributes()) for (var a = 0; a < e.attributes.length; a++) {
                        var o = e.attributes.item(a);
                        "xmlns" != o.localName.toLocaleLowerCase() && r.setAttribute(o.localName, o.value);
                    }
                    if (e.hasChildNodes()) for (var l = 0; l < e.childNodes.length; l++) {
                        var s = e.childNodes.item(l);
                        if (4 === s.nodeType) r.appendChild(n.createCDATASection(s.nodeValue)); else if (3 === s.nodeType) r.appendChild(n.createTextNode(s.nodeValue)); else if (1 === s.nodeType) {
                            var u = t(s);
                            r.appendChild(u);
                        }
                    }
                    return r;
                }
                var r = new DOMParser().parseFromString(e, "text/xml"), n = document.implementation.createDocument("", "", null), a = t(r.documentElement);
                return n.appendChild(a), n;
            }
        };
        t["default"] = r, e.exports = t["default"];
    }
});