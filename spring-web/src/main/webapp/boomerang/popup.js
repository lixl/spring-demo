var id = chrome.app.getDetails().id;
var htmlPage = "/workspace.html";
var tabUrl = "chrome-extension://" + id + htmlPage;
var createNewTab = true;

chrome.windows.getCurrent(function (currentWindow) {
    var tabWindows = chrome.extension.getViews({ type: "tab", windowId: currentWindow.id });

    tabWindows.forEach(function (tabWindow) {
        if (tabWindow.location.hostname === id && createNewTab) {
            createNewTab = false;
            chrome.tabs.update(tabWindow.chromeTabId, { active: true });
            window.close();
        }
    });

    if (createNewTab) {
        chrome.tabs.create({
            url: tabUrl
        });
    }

});





