package com.sysware;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.sysware.user.LocalFileService;

public class BootStrapService implements ServletContextListener{
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		LocalFileService.loadFiles();
	}

}
