package com.sysware.user;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sysware.p2m.httpclient.P2MHttpClientUtil;

@RequestMapping("userAction")
@Controller
public class UserAction {
	
	@Qualifier("restTemplate")
	@Autowired
	RestTemplate restTemplate;
	
	String url = "http://192.168.1.104:8080/spring-web/userAction/doLogin.action";
	
	@RequestMapping("restTemplate")
	@ResponseBody
	public String restTemplate(HttpServletRequest request) throws Exception {
		System.out.println(request.getParameter("username"));
		return restTemplate.getForObject(url, String.class);
	}
	
	@RequestMapping("httpClient")
	@ResponseBody
	public String httpClient(HttpServletRequest request) throws Exception {
		System.out.println(request.getParameter("username"));
		JSONObject obj = P2MHttpClientUtil.doGet(url);
		return obj.toJSONString();
	}

	@RequestMapping("doLogin")
	@ResponseBody
	public String doLogin(HttpServletRequest request) throws Exception {
		System.out.println(request.getParameter("username"));
		JSONObject obj = new JSONObject();
		obj.put("success", true);
		obj.put("message", "login success!");
		return obj.toString();
	}

	@RequestMapping("toLoginPage")
	public String toLoginPage() {

		return "login";
	}

	@RequestMapping("getDatas")
	@ResponseBody
	public String getDatas(HttpServletRequest request) {
		JSONArray datas = LocalFileService.getFilesFromLog();
		// JSONArray obj = LocalFileService.getFiles();
		return datas.toString();
	}

	@RequestMapping(value = "getFileContents")
	public void getFileContents(String id, final HttpServletResponse response) throws Exception {
		File file = LocalFileService.getFile(id);
		if (file == null)
			return;
		String fileName = URLEncoder.encode(file.getName(), "UTF-8");
		response.reset();
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
		response.addHeader("Content-Length", "" + file.length());
		response.setContentType("application/octet-stream;charset=UTF-8");

		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
		OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
		byte[] buffer = new byte[1024 * 1024 * 4];
		int i = -1;
		while ((i = bis.read(buffer)) != -1) {
			toClient.write(buffer, 0, i);
		}
		bis.close();
		toClient.flush();
		toClient.close();
	}

}
