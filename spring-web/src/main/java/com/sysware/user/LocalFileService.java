package com.sysware.user;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.core.io.ClassPathResource;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class LocalFileService {
	private static ConcurrentHashMap<String, File> ALL_FILES = new ConcurrentHashMap<String, File>();

	public static void loadFiles() {
		listFiles(new File(""));
	}

	private static String[] SUFFIX_NAMES = { "mp4", "avi", "rmvb", "mkv" };

	private static void listFiles(File file) {
		if (file != null && file.isDirectory() && file.listFiles() != null) {
			for (File f : file.listFiles()) {
				listFiles(f);
			}
		} else if (file != null && file.isFile()
				&& ArrayUtils.contains(SUFFIX_NAMES, FilenameUtils.getExtension(file.getName()))) {
			ALL_FILES.put(RandomStringUtils.randomNumeric(32), file);
		}
	}

	public static JSONArray getFiles() {
		JSONArray datas = new JSONArray();
		for (Map.Entry<String, File> e : ALL_FILES.entrySet()) {
			if (e.getValue() == null)
				continue;
			JSONObject obj = new JSONObject();
			obj.put("id", e.getKey());
			String name = FilenameUtils.getBaseName(e.getValue().getName());
			try {
				name = URLEncoder.encode(name, "UTF-8");
			} catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			}
			obj.put("name", name);
			datas.add(obj);
		}
		return datas;
	}

	public static JSONArray getFilesFromLog() {
		JSONArray datas = new JSONArray();
		try {
			File file = getLogFile();
			if(file==null || !file.exists()) return datas;
			List<String> list = FileUtils.readLines(file, "UTF-8");
			if (list != null) {
				for (String line : list) {
					if (line != null && line.length() > 0) {
						JSONObject data = JSONObject.parseObject(line);
						if (data != null) {
							for (Map.Entry<String, Object> e : data.entrySet()) {
								if (e != null) {
									JSONObject obj = new JSONObject();
									obj.put("name", e.getKey());
									String oname = FilenameUtils.getBaseName("" + e.getValue());
									try {
										oname = URLEncoder.encode(oname, "UTF-8");
									} catch (UnsupportedEncodingException e1) {
										e1.printStackTrace();
									}
									obj.put("oname", oname);
									datas.add(obj);
								}
							}
						}
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return datas;
	}
	
	public static File getLogFile() throws IOException {
		ClassPathResource resource = new ClassPathResource("log.txt");
		if(resource.exists()) {
			return resource.getFile();
		}
		return null;
	}
	
	public static void main(String[] args) throws IOException {
		System.out.println(getLogFile());
	}

	public static File getFile(String key) {
		return ALL_FILES.get(key);
	}

}
