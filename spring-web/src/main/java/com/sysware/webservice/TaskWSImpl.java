package com.sysware.webservice;

import javax.jws.WebParam;

import com.alibaba.fastjson.JSONObject;

public class TaskWSImpl implements TaskWS {

	@Override
	public String getUserTasks(@WebParam String userId, @WebParam int day) {
		JSONObject obj = new JSONObject();
		obj.put("userId", userId);
		obj.put("day", day + 10);
		
		return obj.toJSONString();
	}

}
