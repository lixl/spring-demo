package com.sysware.webservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(serviceName="testTaskWS")
public interface TaskWS {

	@WebMethod
	public String getUserTasks(@WebParam String userId, @WebParam int day);

}
