package com.sysware.p2m.httpproxy;

import java.io.IOException;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.client.utils.URIUtils;

@WebServlet(urlPatterns = "/demo/*", initParams = {
		@WebInitParam(name = ProxyServlet.P_TARGET_URI, value = "http://localhost:8080/demo") })
public class DemoProxyServlet extends ProxyServlet {

	private static final long serialVersionUID = 1L;

	@Override
	public void init() throws ServletException {
		super.init();
		targetUri = System.getProperty("sysware.p2m.demo");
		if (targetUri == null)
			throw new ServletException(P_TARGET_URI + " is required.");
		// test it's valid
		try {
			targetUriObj = new URI(targetUri);
		} catch (Exception e) {
			throw new ServletException("Trying to process targetUri init parameter: " + e, e);
		}
		targetHost = URIUtils.extractHost(targetUriObj);
	}
	
	@Override
	protected void service(HttpServletRequest servletRequest, HttpServletResponse servletResponse)
			throws ServletException, IOException {
		super.service(servletRequest, servletResponse);
	}

}
