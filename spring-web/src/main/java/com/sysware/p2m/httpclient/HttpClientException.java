package com.sysware.p2m.httpclient;
/**
 * 
 * @copyright    : Sysware Technology Co., Ltd
 *
 * 描述：HttpClient异常报错
 * @version     : 1.0 
 * @since       : 2018年5月18日上午9:26:45
 * @team	    : P2M组
 * @author      : dujg
 */
public class HttpClientException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8273586782303965663L;

	public HttpClientException() {
	}

	public HttpClientException(String message) {
		super(message);
	}

	public HttpClientException(Throwable cause) {
		super(cause);
	}

	public HttpClientException(String message, Throwable cause) {
		super(message, cause);
	}

}
