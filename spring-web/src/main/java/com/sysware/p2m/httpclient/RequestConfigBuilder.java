package com.sysware.p2m.httpclient;

import org.apache.http.client.config.RequestConfig;

public class RequestConfigBuilder {

	private int connectTimeout;
	private int socketTimeout;
	private int connectionRequestTimeout;

	public RequestConfig build() {
		RequestConfig.Builder configBuilder = RequestConfig.custom();
		// 设置连接超时
		configBuilder.setConnectTimeout(this.getConnectTimeout());
		// 设置读取超时
		configBuilder.setSocketTimeout(this.getSocketTimeout());
		// 设置从连接池获取连接实例的超时
		configBuilder.setConnectionRequestTimeout(this.getConnectionRequestTimeout());
		return configBuilder.build();
	}

	public int getConnectTimeout() {
		return connectTimeout;
	}

	public void setConnectTimeout(int connectTimeout) {
		this.connectTimeout = connectTimeout;
	}

	public int getSocketTimeout() {
		return socketTimeout;
	}

	public void setSocketTimeout(int socketTimeout) {
		this.socketTimeout = socketTimeout;
	}

	public int getConnectionRequestTimeout() {
		return connectionRequestTimeout;
	}

	public void setConnectionRequestTimeout(int connectionRequestTimeout) {
		this.connectionRequestTimeout = connectionRequestTimeout;
	}
}
