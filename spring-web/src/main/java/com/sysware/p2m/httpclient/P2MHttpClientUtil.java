package com.sysware.p2m.httpclient;

import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 *
 * @copyright : Sysware Technology Co., Ltd
 *
 *            描述： 支持连接池Client，提高性能
 * @version : 1.0
 * @since : 2018年5月18日上午10:16:30
 * @team : P2M组
 * @author : dujg
 */
public class P2MHttpClientUtil {
	private static Logger log = Logger.getLogger(P2MHttpClientUtil.class);
	private static PoolingHttpClientConnectionManager connMgr;
	private static RequestConfig requestConfig;
	private static final int MAX_TIMEOUT = 7000;

	static {
		// 设置连接池
		connMgr = new PoolingHttpClientConnectionManager();
		// 设置连接池大小
		connMgr.setMaxTotal(100);
		connMgr.setDefaultMaxPerRoute(connMgr.getMaxTotal());
		// 检查链接
		connMgr.setValidateAfterInactivity(1000);
		RequestConfig.Builder configBuilder = RequestConfig.custom();
		// 设置连接超时
		configBuilder.setConnectTimeout(MAX_TIMEOUT);
		// 设置读取超时
		configBuilder.setSocketTimeout(MAX_TIMEOUT);
		// 设置从连接池获取连接实例的超时
		configBuilder.setConnectionRequestTimeout(MAX_TIMEOUT);
		// 在提交请求之前 测试连接是否可用
		// configBuilder.setStaleConnectionCheckEnabled(true);
		requestConfig = configBuilder.build();
	}

	/**
	 * <h1>GET请求</h1>
	 * 
	 * <pre></pre>
	 *
	 * @param url
	 *            请求url地址
	 * @return
	 * @date : 2018年5月18日 09:42:30
	 * @author : dujg
	 */
	public static String get(String url) {
		log.info("GET请求URL：" + url);
		String apiUrl = url;
		String result = null;
		HttpClient httpClient = null;
		if (apiUrl.startsWith("https")) {
			httpClient = HttpClients.custom().setSSLSocketFactory(createSSLConnSocketFactory())
					.setConnectionManager(connMgr).setDefaultRequestConfig(requestConfig).build();
		} else {
			httpClient = HttpClients.custom().setConnectionManager(connMgr).setDefaultRequestConfig(requestConfig)
					.build();
			// httpClient = HttpClients.createDefault();
		}
		HttpResponse response = null;
		try {
			HttpGet httpGet = new HttpGet(apiUrl);
			response = httpClient.execute(httpGet);
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				InputStream instream = entity.getContent();
				result = IOUtils.toString(instream, "UTF-8");
			}
			log.info("GET请求结果：" + result);
			log.info("GET请求状态码：" + response.getStatusLine().getStatusCode());
			return result;
		} catch (Exception e) {
			throw new HttpClientException(getExceptionMessage(e, response));
		}
	}

	/**
	 * <h1>GET请求</h1>
	 * 
	 * <pre></pre>
	 *
	 * @param url
	 *            请求url地址
	 * @return
	 * @date : 2018年5月18日 09:42:30
	 * @author : dujg
	 */
	public static JSONObject doGet(String url) {
		return doGet(url, new HashMap<String, Object>());
	}

	/**
	 * <h1>GET请求</h1>
	 * 
	 * <pre></pre>
	 *
	 * @param url
	 *            请求url地址
	 * @param params
	 *            请求的参数 key-value形式
	 * @return
	 * @date : 2018年5月18日 09:42:30
	 * @author : dujg
	 */
	public static JSONObject doGet(String url, Map<String, Object> params) {
		log.info("GET请求URL：" + url);
		log.info("GET请求参数：" + params.toString());
		String apiUrl = url;
		StringBuffer param = new StringBuffer();
		int i = 0;
		for (String key : params.keySet()) {
			if (i == 0)
				param.append("?");
			else
				param.append("&");
			param.append(key).append("=").append(params.get(key));
			i++;
		}
		apiUrl += param;
		String result = null;
		HttpClient httpClient = null;
		if (apiUrl.startsWith("https")) {
			httpClient = HttpClients.custom().setSSLSocketFactory(createSSLConnSocketFactory())
					.setConnectionManager(connMgr).setDefaultRequestConfig(requestConfig).build();
		} else {
			httpClient = HttpClients.custom().setConnectionManager(connMgr).setDefaultRequestConfig(requestConfig)
					.build();
			// httpClient = HttpClients.createDefault();
		}
		HttpResponse response = null;
		try {
			HttpGet httpGet = new HttpGet(apiUrl);
			response = httpClient.execute(httpGet);
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				InputStream instream = entity.getContent();
				result = IOUtils.toString(instream, "UTF-8");
			}
			log.info("GET请求结果：" + result);
			log.info("GET请求状态码：" + response.getStatusLine().getStatusCode());
			return JSON.parseObject(result);
		} catch (Exception e) {
			throw new HttpClientException(getExceptionMessage(e, response));
		}
	}

	/**
	 * <h1>POST请求 带参数</h1>
	 * 
	 * <pre>
	 * 注意此方法不支持服务端用@RequestBody接参数
	 * </pre>
	 *
	 * @param apiUrl
	 *            请求url地址
	 * @return
	 * @date : 2018年5月18日 09:40:32
	 * @author : dujg
	 */
	public static String post(String apiUrl, Map<String, Object> params) {
		return doPost(apiUrl, params).toJSONString();
	}

	/**
	 * <h1>POST请求 不带参数</h1>
	 * 
	 * <pre></pre>
	 *
	 * @param apiUrl
	 *            请求url地址
	 * @return
	 * @date : 2018年5月18日 09:40:32
	 * @author : dujg
	 */
	public static JSONObject doPost(String apiUrl) {
		return doPost(apiUrl, new HashMap<String, Object>());
	}

	/**
	 * <h1>POST请求 带参数</h1>
	 * 
	 * <pre>
	 * 注意此方法不支持服务端用@RequestBody接参数
	 * </pre>
	 *
	 * @param apiUrl
	 *            请求url地址
	 * @param params
	 *            key-value形式
	 * @return
	 * @date :2018年5月18日 09:40:56
	 * @author : dujg
	 */
	public static JSONObject doPost(String apiUrl, Map<String, Object> params) {
		log.info("POST请求URL：" + apiUrl);
		log.info("POST请求参数：" + params.toString());
		CloseableHttpClient httpClient = null;
		if (apiUrl.startsWith("https")) {
			httpClient = HttpClients.custom().setSSLSocketFactory(createSSLConnSocketFactory())
					.setConnectionManager(connMgr).setDefaultRequestConfig(requestConfig).build();
		} else {
			httpClient = HttpClients.custom().setConnectionManager(connMgr).setDefaultRequestConfig(requestConfig)
					.build();
			// httpClient = HttpClients.createDefault();
		}
		String httpStr = null;
		HttpPost httpPost = new HttpPost(apiUrl);
		CloseableHttpResponse response = null;

		try {
			httpPost.setConfig(requestConfig);
			List<NameValuePair> pairList = new ArrayList<>(params.size());
			for (Map.Entry<String, Object> entry : params.entrySet()) {
				NameValuePair pair = new BasicNameValuePair(entry.getKey(), entry.getValue().toString());
				pairList.add(pair);
			}
			httpPost.setEntity(new UrlEncodedFormEntity(pairList, Charset.forName("UTF-8")));
			response = httpClient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			httpStr = EntityUtils.toString(entity, "UTF-8");
			log.info("POST请求结果：" + httpStr);
			log.info("POST请求状态码：" + response.getStatusLine().getStatusCode());
			return JSON.parseObject(httpStr);
		} catch (Exception e) {
			throw new HttpClientException(getExceptionMessage(e, response));
		} finally {
			if (response != null) {
				try {
					EntityUtils.consume(response.getEntity());
				} catch (Exception e) {
					throw new HttpClientException(getExceptionMessage(e, response));
				}
			}
		}

	}

	/**
	 * <h1>application/json方式POST请求</h1>
	 * 
	 * <pre>
	 * 服务端可以用@RequestBody接收参数
	 * </pre>
	 *
	 * @param apiUrl
	 *            请求url
	 * @param json
	 *            json格式参数
	 * @return
	 * @throws Exception
	 * @date : 2018年5月18日 09:41:35
	 * @author : dujg
	 */
	public static String post(String apiUrl, String json) {
		return doPost(apiUrl, json).toJSONString();
	}

	/**
	 * <h1>application/json方式POST请求</h1>
	 * 
	 * <pre>
	 * 服务端可以用@RequestBody接收参数
	 * </pre>
	 *
	 * @param apiUrl
	 *            请求url
	 * @param json
	 *            json格式参数
	 * @return
	 * @throws Exception
	 * @date : 2018年5月18日 09:41:35
	 * @author : dujg
	 */
	public static JSONObject doPost(String apiUrl, String json) {
		log.info("JSONPOST请求URL：" + apiUrl);
		log.info("JSONPOST请求参数：" + json);
		CloseableHttpClient httpClient = null;
		if (apiUrl.startsWith("https")) {
			httpClient = HttpClients.custom().setSSLSocketFactory(createSSLConnSocketFactory())
					.setConnectionManager(connMgr).setDefaultRequestConfig(requestConfig).build();
		} else {

			httpClient = HttpClients.custom().setConnectionManager(connMgr).setDefaultRequestConfig(requestConfig)
					.build();
			// httpClient = HttpClients.createDefault();
		}
		String httpStr = null;
		HttpPost httpPost = new HttpPost(apiUrl);
		CloseableHttpResponse response = null;
		try {
			httpPost.setConfig(requestConfig);
			StringEntity stringEntity = new StringEntity(json, "UTF-8");// 解决中文乱码问题
			stringEntity.setContentEncoding("UTF-8");
			httpPost.setEntity(stringEntity);
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json; charset=UTF-8");
			response = httpClient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			httpStr = EntityUtils.toString(entity, "UTF-8");
			log.info("JSONPOST请求结果：" + httpStr);
			log.info("JSONPOST请求状态码：" + response.getStatusLine().getStatusCode());
			return JSON.parseObject(httpStr);
		} catch (Exception e) {
			throw new HttpClientException(getExceptionMessage(e, response));
		} finally {
			if (response != null) {
				try {
					EntityUtils.consume(response.getEntity());
				} catch (Exception e) {
					throw new HttpClientException(getExceptionMessage(e, response));
				}
			}
		}
	}

	/**
	 * <h1>application/xml方式POST请求</h1>
	 * 
	 * <pre>
	 * 服务端可以用@RequestBody接收参数
	 * </pre>
	 *
	 * @param apiUrl
	 *            请求url
	 * @param xml
	 *            xml格式参数
	 * @return
	 * @throws Exception
	 * @date : 2018年5月18日 09:41:35
	 * @author : dujg
	 */
	public static JSONObject doPostXML(String apiUrl, String xml) {
		log.info("XMLPOST请求URL：" + apiUrl);
		log.info("XMLPOST请求参数：" + xml);
		CloseableHttpClient httpClient = null;
		if (apiUrl.startsWith("https")) {
			httpClient = HttpClients.custom().setSSLSocketFactory(createSSLConnSocketFactory())
					.setConnectionManager(connMgr).setDefaultRequestConfig(requestConfig).build();
		} else {
			httpClient = HttpClients.custom().setConnectionManager(connMgr).setDefaultRequestConfig(requestConfig)
					.build();
			// httpClient = HttpClients.createDefault();
		}
		String httpStr = null;
		HttpPost httpPost = new HttpPost(apiUrl);
		CloseableHttpResponse response = null;

		try {
			httpPost.setConfig(requestConfig);
			StringEntity stringEntity = new StringEntity(xml, "UTF-8");// 解决中文乱码问题
			stringEntity.setContentEncoding("UTF-8");
			httpPost.setEntity(stringEntity);
			httpPost.setHeader("Accept", "application/xml");
			httpPost.setHeader("Content-type", "application/xml; charset=UTF-8");
			response = httpClient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			httpStr = EntityUtils.toString(entity, "UTF-8");
			log.info("XMLPOST请求结果：" + httpStr);
			log.info("XMLPOST请求状态码：" + response.getStatusLine().getStatusCode());
			return JSON.parseObject(httpStr);
		} catch (Exception e) {
			throw new HttpClientException(getExceptionMessage(e, response));
		} finally {
			if (response != null) {
				try {
					EntityUtils.consume(response.getEntity());
				} catch (Exception e) {
					throw new HttpClientException(getExceptionMessage(e, response));
				}
			}
		}
	}

	public static String postXml(String apiUrl, String xml) {
		log.info("XMLPOST请求URL：" + apiUrl);
		log.info("XMLPOST请求参数：" + xml);
		CloseableHttpClient httpClient = null;
		if (apiUrl.startsWith("https")) {
			httpClient = HttpClients.custom().setSSLSocketFactory(createSSLConnSocketFactory())
					.setConnectionManager(connMgr).setDefaultRequestConfig(requestConfig).build();
		} else {
			httpClient = HttpClients.custom().setConnectionManager(connMgr).setDefaultRequestConfig(requestConfig)
					.build();
			// httpClient = HttpClients.createDefault();
		}
		String httpStr = null;
		HttpPost httpPost = new HttpPost(apiUrl);
		CloseableHttpResponse response = null;

		try {
			httpPost.setConfig(requestConfig);
			StringEntity stringEntity = new StringEntity(xml, "UTF-8");// 解决中文乱码问题
			stringEntity.setContentEncoding("UTF-8");
			httpPost.setEntity(stringEntity);
			httpPost.setHeader("Accept", "application/xml");
			httpPost.setHeader("Content-type", "application/xml; charset=UTF-8");
			response = httpClient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			httpStr = EntityUtils.toString(entity, "UTF-8");
			log.info("XMLPOST请求结果：" + httpStr);
			log.info("XMLPOST请求状态码：" + response.getStatusLine().getStatusCode());
			return httpStr;
		} catch (Exception e) {
			throw new HttpClientException(getExceptionMessage(e, response));
		} finally {
			if (response != null) {
				try {
					EntityUtils.consume(response.getEntity());
				} catch (Exception e) {
					throw new HttpClientException(getExceptionMessage(e, response));
				}
			}
		}
	}

	/**
	 * 创建SSL安全连接(https)
	 */
	private static SSLConnectionSocketFactory createSSLConnSocketFactory() {
		SSLConnectionSocketFactory sslsf = null;
		try {
			SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {

				public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
					return true;
				}
			}).build();
			sslsf = new SSLConnectionSocketFactory(sslContext, new HostnameVerifier() {

				@Override
				public boolean verify(String arg0, SSLSession arg1) {
					return true;
				}
			});
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		}
		return sslsf;
	}

	private static String getExceptionMessage(Exception e, HttpResponse response) {
		e.printStackTrace();
		String msg = null;
		if (e instanceof org.apache.http.conn.HttpHostConnectException) {
			msg = "连接地址存在问题，请检查！:" + e.getMessage();
		} else if (e instanceof java.lang.IllegalArgumentException) {
			msg = "端口或者ip错误，请检查！:" + e.getMessage();
		} else if (e instanceof java.net.UnknownHostException) {
			msg = "端口或者ip错误，请检查！:" + e.getMessage();
		} else if (e instanceof java.net.SocketTimeoutException) {
			msg = "请求超时！:" + e.getMessage();
		} else if (e instanceof com.alibaba.fastjson.JSONException) {
			msg = "返回结果集非json格式，请检查！:" + e.getMessage();
		} else {
			msg = "其它异常！:" + e.getMessage();
		}
		if (response != null) {
			msg += "HTTP状态码：" + response.getStatusLine().getStatusCode();
		}
		return msg;
	}

	/**
	 * 上传文件
	 * 
	 * @param fileName
	 * @param file
	 * @param upLoadPath
	 * @return
	 */
	public static JSONObject doPostFile(String fileName, File file, String upLoadPath) {
		MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();
		multipartEntityBuilder.addBinaryBody("file", file, ContentType.MULTIPART_FORM_DATA, fileName);
		return postHttpEntity(multipartEntityBuilder.build(), upLoadPath);
	}

	/**
	 * 上传文件流
	 * 
	 * @param fileName
	 * @param inputStream
	 * @param upLoadPath
	 * @return
	 */
	public static JSONObject doPostFileInputStream(String fileName, InputStream inputStream, String upLoadPath) {
		MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();
		ContentType contentType = ContentType.create("multipart/form-data", Charset.forName("UTF-8"));
		multipartEntityBuilder.addBinaryBody("file", inputStream, contentType, fileName);
		return postHttpEntity(multipartEntityBuilder.build(), upLoadPath);
	}

	private static JSONObject postHttpEntity(HttpEntity httpEntity, String upLoadPath) {
		HttpPost httpPost = new HttpPost(upLoadPath);
		httpPost.setConfig(requestConfig);
		httpPost.setEntity(httpEntity);
		CloseableHttpClient httpClient = createCloseableHttpClient(upLoadPath);

		HttpResponse response = null;
		try {
			response = httpClient.execute(httpPost);
		} catch (Exception e) {
			e.printStackTrace();
		}
		JSONObject result = null;
		if (response != null) {
			HttpEntity responseEntity = response.getEntity();
			String res = null;
			try {
				res = EntityUtils.toString(responseEntity, Charset.forName("UTF-8"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				if (!SyswareUtil.isEmpty(res)) {
					result = JSONObject.parseObject(res);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	private static CloseableHttpClient createCloseableHttpClient(String apiUrl) {
		CloseableHttpClient httpClient = null;
		if (apiUrl.startsWith("https")) {
			httpClient = HttpClients.custom().setSSLSocketFactory(createSSLConnSocketFactory())
					.setConnectionManager(connMgr).setDefaultRequestConfig(requestConfig).build();
		} else {
			httpClient = HttpClients.custom().setConnectionManager(connMgr).setDefaultRequestConfig(requestConfig)
					.build();
		}
		return httpClient;
	}
}