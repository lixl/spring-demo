package com.sysware.utils;

public class ReturnMessage {
	private boolean success;
	private String data;

	public ReturnMessage() {
		// TODO Auto-generated constructor stub
	}

	public ReturnMessage(boolean success, String data) {
		super();
		this.success = success;
		this.data = data;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
