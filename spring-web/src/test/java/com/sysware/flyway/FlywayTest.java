package com.sysware.flyway;

import java.io.IOException;
import java.util.Properties;

import org.flywaydb.core.Flyway;
import org.junit.Test;
import org.springframework.core.io.support.PropertiesLoaderUtils;

public class FlywayTest {
	
	@Test
	public void migrate() throws IOException {
		Properties properties = PropertiesLoaderUtils.loadAllProperties("flyway.conf");
		Flyway flyway = new Flyway();
		flyway.configure(properties);
		flyway.migrate();
	}

}
