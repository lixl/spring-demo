
  CREATE TABLE "CONFIG_APP_SET" 
   (	"ID" VARCHAR2(40 BYTE) NOT NULL ENABLE, 
	"NAME" VARCHAR2(400 CHAR), 
	"DESCRIPTION" VARCHAR2(4000 CHAR), 
	"VALUE" VARCHAR2(400 BYTE), 
	"CONFIGTYPE" VARCHAR2(400 BYTE) NOT NULL ENABLE, 
	"JSONEXTEND" VARCHAR2(4000 BYTE), 
	"ROWSTATUSID" VARCHAR2(400 BYTE), 
	"TIME" TIMESTAMP (6), 
	 CONSTRAINT "C_A_S" PRIMARY KEY ("ID")
   );
 
