package com.tocmat;

import javax.servlet.ServletException;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.startup.Tomcat;

public class Launcher {

	public static void main(String[] args) throws ServletException, LifecycleException {
		Tomcat tomcat = new Tomcat();
		Connector connector = tomcat.getConnector();
		connector.setMaxHeaderCount(20000);
		connector.setPort(8888);
		connector.setEnableLookups(false);
		//connector.setParseBodyMethods("get");
		
		tomcat.setConnector(connector);
		tomcat.addWebapp("/localrepo", "D:\\develop\\workspace-oxygen\\syswareFramework\\WebRoot");
		
		tomcat.start();
		tomcat.getServer().await();
	}
}
