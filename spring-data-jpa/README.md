# 数据库操作
## mysql
```sql
-- 1. 创建数据库
create database testdb default charset utf8 collate utf8_general_ci;
-- 2.创建用户及授权
grant all privileges on testdb.* to testdbuser@'%' identified by 'testdbuser';
-- with grant option 可以给其他用户授权
flush privileges; --更新权限后才能正常使用

-- 3.删除用户
drop user testdbuser;

-- 4.删除数据库
drop database testdb;
```
## oracle
```sql
CREATE USER testdbuser IDENTIFIED BY testdbuser ACCOUNT UNLOCK;
GRANT CONNECT, RESOURCE, CREATE VIEW TO testdbuser;

--删除用户
DROP USER testdbuser CASCADE;
```
## h2

# jpa文档
https://github.com/spring-projects/spring-data-jpa      
https://github.com/spring-projects/spring-data-examples/tree/master/jpa

