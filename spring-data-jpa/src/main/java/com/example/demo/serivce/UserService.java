package com.example.demo.serivce;

import com.example.demo.entity.User;

import java.util.List;

public interface UserService {

    Long count();

    void save(User user);

    List<User> getUser(String queryName, int start, int limit);

    Long getUserCount(String queryName);

}
