package com.example.demo.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
@Data
public class User {
    @Id //主键
    private String user_id;
    private Integer user_age;
    private String user_name;
    private String user_address;
    private String user_city;
}