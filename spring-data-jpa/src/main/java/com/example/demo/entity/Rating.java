package com.example.demo.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * 字段	说明
 * userId	用户 id (从 0 开始，连续编号)
 * restId	即 restaurants.csv 中的 restId
 * rating	总体评分，[0,5] 之间的整数
 * rating_env	环境评分，[1,5] 之间的整数
 * rating_flavor	口味评分，[1,5] 之间的整数
 * rating_service	服务评分，[1,5] 之间的整数
 * timestamp	评分时间戳
 * comment	评论内容
 */
@Entity
@Table(name = "ratings")
@Data
public class Rating {
    @Id
    private String rating_id;
    private String user_id;
    private String restaurant_id;
    private Integer rating;
    private Integer rating_env;
    private Integer rating_flavor;
    private Integer rating_service;

    @Lob
    private String rating_comment;

    @Temporal(TemporalType.DATE)
    private Date create_time;//评分时间

}
