package com.example.demo.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 字段	说明
 * restId	餐馆 id (从 0 开始，连续编号)
 * name	餐馆名称
 */
@Entity
@Table(name = "restaurants")
@Data
public class Restaurant {

    @Id
    private String restaurant_id;

    private String restaurant_name;

}
