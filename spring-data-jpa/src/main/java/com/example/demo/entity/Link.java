package com.example.demo.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 字段	说明
 * restId	即 restaurants.csv 和 ratings.csv 中的 restId
 * dianpingId	大众点评网的餐馆编号
 */
@Entity
@Table(name = "links")
@Data
public class Link implements Serializable {

    @Id
    private String link_id;

    private String restaurant_id;

    private String rating_id;

}
