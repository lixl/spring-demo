package com.example.demo.serivce;

import com.example.demo.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:application_core.xml")
public class UserServiceTest {

    @Autowired
    UserService userService;

    @Test
    public void getUserCount() {
        Long count = userService.count();
        System.out.println(count);

        User user = new User();
        user.setUser_id("1");
        user.setUser_name("张三");
        user.setUser_address("北京");
        user.setUser_age(19);
        user.setUser_city("北京");
        userService.save(user);

        System.out.println(user);

        List<User> userList = userService.getUser("name", 5, 5);
        System.out.println(userList);

        count = userService.getUserCount("name");
        System.out.println(count);
    }
}
