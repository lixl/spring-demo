package com.lixl.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lixl.entity.ResultDTO;
import com.lixl.entity.User;

@Api(value = "用户操作", tags = { "普通用户登录", "用户查询" })
@Controller
@RequestMapping("user")
public class UserController {

	@ApiOperation(value = "1001：用户登录")
	@ApiResponses({ @ApiResponse(message = "success=true,登录成功；", code = 200) })
	@RequestMapping(value = "login", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResultDTO login(
			@ApiParam(name = "userName", required = true, value = "用户登录名或手机号") @RequestParam("userName") String userName,
			@ApiParam(name = "password", required = true, value = "登录密码") @RequestParam("password") String password) {

		ResultDTO result = new ResultDTO();

		if (("admin".equals(userName) || "15001321299".equals(userName))
				&& "admin".equals(password)) {
			result.setSuccess(true);
			result.setMessage("认证成功");
			return result;
		}
		result.setMessage("认证失败");
		return result;
	}

	@ApiOperation(value = "1002:根据用户id查询用户信息", httpMethod = "GET", produces = "application/json")
	@ApiResponse(code = 200, message = "success", response = User.class)
	@RequestMapping(value = "getUser", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public User getUser(
			@ApiParam(name = "userId", required = true, value = "用户Id") @RequestParam("userId") int id) {
		User user = new User();
		user.setId(id);
		user.setName("李四");
		user.setAddress("" + new Random().nextDouble());
		return user;
	}
	
	

}
