package com.lixl.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;

import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lixl.entity.User;

@Api(value = "测试专用接口", tags={"hhh"})
@Controller
@RequestMapping("hello")
public class HelloController {

	@ApiResponse(message="hello somebody", code = 200)
	@RequestMapping(value = "say", method = RequestMethod.GET)
	@ResponseBody
	public String sayHello(@RequestParam String name) {

		return "hello " + name;
	}

	@RequestMapping(value = "getUser", method = RequestMethod.GET)
	@ResponseBody
	public User getUser(@RequestParam int id) {
		User user = new User();
		user.setId(id);
		user.setName("李四");
		user.setAddress("" + new Random().nextDouble());
		return user;
	}

	public static void main(String[] args) throws JsonProcessingException {
		ObjectMapper map = new ObjectMapper();
		User user = new User();
		user.setName("李四");
		user.setAddress("" + new Random().nextDouble());
		System.out.println(map.writeValueAsString(user));
	}

}
