package com.sysware.dom4j.demo;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

public class Dom4jDemoApp {

	private static File xmlFile = new File("D://jdom.xml");

	public static void main(String[] args) throws IOException, DocumentException {
		// createFile();
		// readFile();
		// updateValue("1,1", "23");
		long start = System.currentTimeMillis();
		System.out.println(getValue("1,1", "name"));
		System.out.println(System.currentTimeMillis() - start);
	}

	public static void updateValue(File xmlFile, String id, String key, String value) {
		SAXReader reader = new SAXReader();
		try {
			Document document = reader.read(xmlFile);
			Iterator<Element> it = document.getRootElement().elementIterator();
			while (it.hasNext()) {
				Element ele = it.next();
				if (ele == null)
					continue;
				Element idElement = ele.element("id");
				if (idElement == null || idElement.getText() == null)
					continue;
				if (idElement.getText().equals(id)) {
					Element keyElement = ele.element(key);
					if (keyElement != null) {
						keyElement.setText(value);
					}
					break;
				}
			}
			toFile(document, new File("d://dom4j_output.xml"), "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getValue(String id, String key) {
		SAXReader reader = new SAXReader();
		try {
			Document document = reader.read(xmlFile);
			Node dataNode = document.selectSingleNode("/children/data[id='" + id + "']");
			if (dataNode == null)
				return null;
			Object obj = dataNode.selectObject(key);
			if (obj == null)
				return null;
			Element ele = (Element) obj;
			return ele.getText();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void readFile() throws DocumentException {
		SAXReader reader = new SAXReader();
		Document document = reader.read(xmlFile);
		List<Element> list = document.getRootElement().elements();
		for (Element e : list) {

			List<Element> list2 = e.elements();
			for (Element element : list2) {
				System.out.println(element.getName() + " " + element.getText());
			}

		}
	}

	public static void createFile() throws IOException {
		Document document = DocumentFactory.getInstance().createDocument();
		Element children = document.addElement("children");
		for (int i = 0; i < 5000; i++) {
			for (int j = 0; j < 5000; j++) {
				Element data = children.addElement("data");
				data.addElement("id").setText("" + i + "," + j);
				data.addElement("name").setText("name-[" + i + "][" + j + "]");
				int value = i * j;
				data.addElement("value").setText("" + value);
			}
		}

		toFile(document, xmlFile, "utf-8");
	}

	public static void toFile(Document document, File file, String encoding) throws IOException {
		OutputFormat format = OutputFormat.createPrettyPrint();
		format.setEncoding(encoding);
		FileWriter fileWriter = new FileWriter(file);
		XMLWriter writer = new XMLWriter(fileWriter, format);
		writer.write(document);
		writer.close();
		fileWriter.close();
	}

}
