package com.sysware.dom4j.demo;

import java.io.File;

public class AppTest {

	public static void main(String[] args) {
		File xmlFile = new File("D://jdom.xml");
		String id = "1,1";
		String key = "name";
		String value = "value121";
		long t1 = 0;
		long t2 = 0;
		int times = 2;
		for (int i = 0; i < times; i++) {
			long s1 = System.currentTimeMillis();
			Dom4jDemoApp.updateValue(xmlFile, id, key, value);
			t1 += (System.currentTimeMillis() - s1);

			long s2 = System.currentTimeMillis();
			JDomDemo.updateValue(xmlFile, id, key, value);
			t2 += (System.currentTimeMillis() - s2);
		}

		System.err.println("Dom4jDemoApp = " + (t1 / times));
		System.err.println("JDomDemo = " + (t2 / times));
	}

}
